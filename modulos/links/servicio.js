const { getDb } = require("../../mongodb");

const relatedFields = {
    "events": {
        "happenedIn": "",
        "involvedIndividuals": "",
        "involvedSocieties": "", 
        "involvedTreasure":"",
    },
    "locations": {
        "relatedLocations": "relatedLocations",
        "relatedIndividuals": "usualLocations",
        "relatedSocieties": "relatedLocations", 
        "notableTreasure":"possibleLocations",
    },
    "individuals": {
        "usualLocations": "relatedIndividuals",
        "relatedIndividuals": "relatedIndividuals", // usualIndividuals, mejor
        "associatedSocieties": "associatedIndividuals", 
        "notableInventory":"possibleIndividualOwners",
    },
    "societies": {
        "relatedLocations": "relatedSocieties",
        "associatedIndividuals": "associatedSocieties",
        "relatedSocieties": "relatedSocieties", 
        "notableTreasure":"possibleSocietyOwners",
    },
    "objects": {
        "possibleLocations": "notableTreasure",
        "possibleIndividualOwners": "notableInventory",
        "possibleSocietyOwners": "notableTreasure", 
    },
}

async function createLinks(entry, linksEntry){ return new Promise(async (resolve, reject) => {

    console.log(entry)
    const timestamp = Date.now()
    const allLinks = []
    for (const key of Object.keys(linksEntry)) {
        for (const linkInfo of linksEntry[key]) {
            const link = {
                field: key, //field1: key,
                // field2: relatedFields[entry.entryClass][key],
                id1: entry._id,
                id2: linkInfo["id"],
                name1: entry.name,
                name2: linkInfo["name"],
                entryClass1: entry.entryClass,
                entryClass2: linkInfo["entryClass"],
                created_ts: timestamp
            }
            allLinks.push( link )
        }
    }

    await getDb().collection('links').insertMany(allLinks).catch( error => {
        console.error(error);
        reject(error);
    })
    resolve();
})}

async function getLinks(){ return new Promise((resolve, reject) => {
    console.log("request to get all links");
    getDb().collection('links').find().toArray(function (err, docs) {
        console.log(docs)
        resolve (docs);
    })
})}

async function getEntryLinks( entryId ){ return new Promise((resolve, reject) => {
    console.log(`request to get links created by an entry with id ${entryId}`);
    getDb().collection('links').find(
        {"id1":entryId}
    ).toArray(function (err, docs) {
        console.log(docs)
        resolve (docs);
    })
})}

async function getRelatedLinks( entryId ){ return new Promise((resolve, reject) => {
    console.log(`request to get links related to an entry with id ${entryId}`);
    getDb().collection('links').find(
        { $or: [
            {"id1":entryId}, {"id2":entryId}
        ] }
        // { $and: [ {
        //     $or: [ {"entryClass1":{entryClass}}, {"entryClass2":{entryClass}} ],
        //     $or: [ {"id1":{entryId}}, {"id2":{entryId}} ]
        // } ] }
    ).toArray(function (err, docs) {
        console.log(docs)
        resolve (docs);
    })
})}

module.exports = { createLinks, getLinks, getEntryLinks, getRelatedLinks }