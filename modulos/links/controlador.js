const service = require('./servicio')
const {logger} = require('../../logger')

async function create(req, res){
    try{
        const linksEntry = req.body.linksEntry
        const entry = req.body.entry
        logger.info({ action: 'Request to create links' })
        await service.createLinks(entry, linksEntry)
        logger.info({ action: 'Links created' })
        res.writeHead(201, 'Content-Type', 'application/json')
        res.end()
    } catch {
        logger.info({ action: 'Fail in links creation' })
        res.writeHead(500, 'Content-Type', 'application/json')
        res.end(JSON.stringify({ result:"Fail in links creation"}))
    }
}

async function getAll(req, res){

    logger.info({ action: 'request to get all links'})

    const links = await service.getLinks()
    console.log("controller: " + links)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(links))
}

async function getEntry(req, res){

    logger.debug({ action: 'request to get links created by an entry'})
    const entryId = req.body.entryId
    // const entryClass = req.body.entryClass

    const objectsFound = await service.getEntryLinks(entryId)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
}

async function getRelated(req, res){

    logger.debug({ action: 'request to get links related to an entry'})
    const entryId = req.body.entryId
    // const entryClass = req.body.entryClass

    const objectsFound = await service.getRelatedLinks(entryId)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
}

module.exports = { create, getAll, getEntry, getRelated }