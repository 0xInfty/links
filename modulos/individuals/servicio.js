const { ObjectID } = require("bson");
const { getDb } = require("../../mongodb");

const verbosityLevel = 1;
const consoleLogDebug = (stringToConsoleLog, importanceLevel=0) => {
    if ( importanceLevel <= verbosityLevel ) { console.log( stringToConsoleLog ) }
}

async function createIndividualRace( individualRace ) { return new Promise(async (resolve, reject) => { 

    const individualsRacesDocuments = await getDb().collection('races').findOne({name: individualRace})

    if(!individualsRacesDocuments) {
        getDb().collection('races').insertOne({name: individualRace}).then( () => {   
            consoleLogDebug(`Individual Race successfully added: ${individualRace}`, 1)
            resolve ({'resultIndividualRace':'success' })
        }).catch( error => {
            consoleLogDebug(`Individual Race not successfully added: ${individualRace}`)
            console.error(error)
            reject(error)
        })   
    } else{
        resolve ({'resultIndividualRace':'success' })
    }
})}

async function createIndividualOccupation( individualOccupation ) { return new Promise(async (resolve, reject) => { 

    const individualsOccupationsDocuments = await getDb().collection('occupations').findOne({name: individualOccupation})

    if(!individualsOccupationsDocuments) {
        getDb().collection('occupations').insertOne({name: individualOccupation}).then( () => {   
            consoleLogDebug(`Individual Occupation successfully added: ${individualOccupation}`, 1)
            resolve ({'resultIndividualOccupation':'success' })
        }).catch( error => {
            consoleLogDebug(`Individual Occupation not successfully added: ${individualOccupation}`)
            console.error(error)
            reject(error)
        })   
    } else{
        resolve ({'resultIndividualOccupation':'success' })
    }
})}

async function createIndividual(individual){ return new Promise(async (resolve, reject) => {

    individual.created_ts = Date.now()
    const entryId = ObjectID()
    individual._id = entryId
    const entryIdString = ObjectID(entryId).toString()
    consoleLogDebug(entryId, 1)

    await getDb().collection('individuals').insertOne(individual).catch( error => {
        console.error(error);
        reject(error);
    })

    const individualRace = individual.race;
    await createIndividualRace(individualRace).catch( error => {
        console.error(error);
        reject(error);
    })

    const individualOccupation = individual.occupation;
    await createIndividualOccupation(individualOccupation).catch( error => {
        console.error(error);
        reject(error);
    })

    consoleLogDebug(entryIdString, 1)
    resolve( entryIdString )
})}

async function getIndividualById( entryId ){ return new Promise((resolve, reject) => {
    consoleLogDebug(`request to search for specific individual with id '${entryId}'"`, 1);
    let result = getDb().collection('individuals').find({ "_id": ObjectID(entryId)})
    resolve (result);
    })
}

async function getIndividuals(){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to get all individuals", 1);
    getDb().collection('individuals').find().toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "individuals" }
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

async function getIndividualRaces(){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to get all individual races", 1);
    getDb().collection('races').find().toArray(function (err, docs) {
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

async function getIndividualOccupations(){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to get all individual occupations", 1);
    getDb().collection('occupations').find().toArray(function (err, docs) {
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

async function search( searchString ){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to search individuals, string: " + searchString, 1);
    getDb().collection('individuals').find({ $text: {$search: searchString}},{ score:{ $meta: "textScore"}}).toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "individuals" }
        resolve (docs);
    })
})}

async function searchRecent( maxNumber ) { return new Promise((resolve, reject) => {
    consoleLogDebug(`request to search the most recent ${maxNumber} individuals`, 1);
    getDb().collection('individuals').find().sort({"created_ts": -1}).limit(maxNumber).toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "individuals" }
        consoleLogDebug(docs, 2);
        resolve (docs);
    })
})}

module.exports = { createIndividual, getIndividualById, getIndividuals, getIndividualRaces, getIndividualOccupations, search, searchRecent }