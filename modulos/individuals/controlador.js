const service = require('./servicio')
const {logger} = require('../../logger')

async function create(req, res){
    try{
        const individual = req.body
        logger.info({ action: 'Request to create individual' })
        entryId = await service.createIndividual(individual)
        logger.info({ action: 'Individual created' })
        res.writeHead(201, 'Content-Type', 'application/json')
        res.end(JSON.stringify(entryId))
    } catch {
        logger.info({ action: 'Fail en individual creation' })
        res.writeHead(500, 'Content-Type', 'application/json')
        res.end(JSON.stringify({ result:"Fail en individual creation"}))
    }
}

async function getById(req, res){

    logger.info({ action: 'request to search for specific individual'})
    const entryId = req.body.entryId

    const individual = await service.getIndividualById(entryId)
    console.log("controller: " + individual)

    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(individual))
}

async function getAll(req, res){

    logger.info({ action: 'request to get individuals'})

    const individuals = await service.getIndividuals()
    console.log("controller: " + individuals)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(individuals))
}

async function getRaces(req, res){

    logger.info({ action: 'request to get races'})

    const individualRaceObjects = await service.getIndividualRaces()
    var individualRaces = []
    for (var piece of individualRaceObjects) {
      individualRaces.push( piece['name'] )
    }
    console.log("controller: " + individualRaces)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(individualRaces))
}

async function getOccupations(req, res){

    logger.info({ action: 'request to get occupations'})

    const individualOccupationObjects = await service.getIndividualOccupations()
    var individualOccupations = []
    for (var piece of individualOccupationObjects) {
      individualOccupations.push( piece['name'] )
    }
    console.log("controller: " + individualOccupations)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(individualOccupations))
}

async function search(req, res){

    logger.debug({ action: 'request to search individual'})
    const searchString = req.body.searchString

    const objectsFound = await service.search(searchString)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
}

async function searchRecent(req, res){

    logger.debug({ action: 'request to search recent individuals'})
    const maxNumber = req.body.maxNumber

    const objectsFound = await service.searchRecent(maxNumber)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
}

module.exports = { create, getById, getAll, getRaces, getOccupations, search, searchRecent }